const express = require('express');
const path = require('path')

const app = express();

//CONFIG DE PUERTO
app.set('port', process.env.PORT || 3030);

//CONFIG MOTOR DE PLANTILLAS - VISTAS
app.set('views', path.join(__dirname, 'views'));
app.engine('ejs', require('ejs').renderFile);
app.set('view engine', 'ejs');

//MIDDLEWARES
app.use(express.urlencoded({extended: true}))
app.use(express.json())

//RUTAS DE LA APLICACION
app.use(require('./routes/rutas'));
app.use(require('./routes/rutasdb'))
app.use(require('./routes/rutas_user'))
app.use(require('./routes/rutas_admin'))

//ARCHIVOS ESTATICOS
app.use(express.static(path.join(__dirname, 'public')))

//SERVIDOR
app.listen(app.get('port'), () =>{
    console.log(`Soy el puerto que funciona, soy el puerto: ${app.get('port')}`);
})