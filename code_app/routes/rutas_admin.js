const express = require('express');
const rutasAdmin = express.Router();

const product_Controller = require('../controllers/adminControllers')


rutasAdmin.get('/admin/index.ejs', product_Controller.listar)
rutasAdmin.post('/registrarProducto', product_Controller.añadir)

module.exports = rutasAdmin;