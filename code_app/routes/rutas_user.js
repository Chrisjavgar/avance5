const express = require('express');
const rutasUsuario = express.Router();

rutasUsuario.get('/_Usuario.ejs', (req, res) =>{
    res.render('_Usuario');
});

module.exports = rutasUsuario;