const express = require('express');
const rutas = express.Router();

const product_Controller = require('../controllers/productContoller')

rutas.get('/', (req, res) =>{
    res.render('index');
});

rutas.get('/login.ejs', (req, res) =>{
    res.render('login');
});

rutas.get('/productCar.ejs', product_Controller.listar);

rutas.get('/nosotros.ejs', (req, res) =>{
    res.render('nosotros');
});

rutas.get('/prueba', (req, res) =>{
    res.render('prueba');
});

rutas.get('/pagar.ejs', (req, res) => {
    res.render('pagar');
});

module.exports = rutas;