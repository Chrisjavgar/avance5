const express = require('express');
const clienteController = require('../controllers/clientesController');

const rutasdb = express.Router();

rutasdb.post('/registrar', clienteController.registrar)
rutasdb.post('/login', clienteController.ingresar)

module.exports = rutasdb