CREATE DATABASE db_security_mastered

USE db_security_mastered

CREATE TABLE `Cliente` (
  `id` int NOT NULL auto_increment,
  `nombre` varchar(25) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `email` varchar(25) NOT NULL,
  `contraseña` varchar(25) NOT NULL,
  PRIMARY KEY  (`id`)
)

CREATE TABLE `Producto` (
  `id` int NOT NULL auto_increment,
  `nom_producto` varchar(25) NOT NULL,
  `precio_producto` int NOT NULL,
  `img_producto` text(255) NOT NULL,
  `desc_producto` text(255) NOT NULL,
  PRIMARY KEY  (`id`)
)

INSERT INTO `producto` (`id`, `nom_producto`, `precio_producto`, `img_producto`, `desc_producto`) VALUES (NULL, 'Camara 1', '100', 'https://1700digital.com/wp-content/uploads/hikvision-1mp-metal.jpg', 'Camara de 720p'), (NULL, 'Camara 2', '200', 'https://www.google.com.ec/url?sa=i&url=https%3A%2F%2Fwww.worldcomputers.com.ec%2Fproducto%2Fcamara-bullet-hikvision-turbo-hd-720p%2F&psig=AOvVaw1KY0Z3jUU7htEJQs28PC31&ust=1628226720981000&source=images&cd=vfe&ved=2ahUKEwjo_-erj5nyAhVOMFMKHU7YB9IQjRx6BAgAEA8', 'Camara de 1080p');