const conn = require('../dbc')

const adminContorller = {}

adminContorller.listar = async (req, res) =>{
    conn.db.query('SELECT * FROM producto', (err, results) =>{
        res.render('./admin/index',{
            datos: results
        })
    })
}

adminContorller.añadir = async (req, res)  =>{
    const { nombreProducto, precioProducto, descProducto, imgProducto} = req.body;
    conn.db.query('INSERT INTO producto SET ?', {
        nom_producto: nombreProducto,
        precio_producto: precioProducto,
        img_producto: imgProducto,
        desc_producto: descProducto
        
    }, (err, reuslt) =>{
        res.redirect('/')
    })
}

module.exports = adminContorller;