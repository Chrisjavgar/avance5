const conn = require('../dbc')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs')

const clienteController = {};

clienteController.registrar = async (req, res) => {
    console.log(req.body);

    const {nomRegistrer, userRegistrer, emailRegistrer, claveRegistrer, confContraseña} = req.body;
    
    if(nomRegistrer == "" || userRegistrer == "" || emailRegistrer == "" || claveRegistrer == "" || confContraseña == ""){
        return res.render('login',{
            alert: true, mensaje: "Campos vacios", icono: "info", time: 1500, ruta: '/login.ejs'
        })
    }

    conn.db.query('SELECT usuario FROM `Cliente` WHERE usuario = ?', [userRegistrer], (err, results) => {
        if(err){
            console.log(err);
        }
        if(results.length > 0){
            return res.render('login', {
                alert: true, mensaje: "Nombre de usuario ya registrado", icono: "warning", time: 1500, ruta: '/login.ejs'
            });
        }
        else if(claveRegistrer !== confContraseña){
            return res.render('login', {
                alert: true, mensaje: "Las contraseñas no coinciden", icono: "question", time: 1500, ruta: '/login.ejs'
            });
        }

        conn.db.query('SELECT email FROM `Cliente` WHERE email = ?', [emailRegistrer], (err, results) =>{
            if(err){
                console.log(err);
            }
            if(results.length > 0){
                return res.render('login', {
                    alert: true, mensaje: "Correo ingresado en uso", icono: "warning", time: 1500, ruta: '/login.ejs'
                });
            }
        })

        //let contraseña_Encrypt = await bcrypt.hash(claveRegistrer, 8)

        conn.db.query('INSERT INTO `Cliente` SET ?', {nombre: nomRegistrer, usuario: userRegistrer, email: emailRegistrer, contraseña: claveRegistrer}, (err, results) => {
            if(err){
                console.log(err)
            }
            else{
                console.log(results)
                return res.render('login', {
                    alert: true, mensaje: "Registro completado con exito", icono: "success", time: 1500, ruta: '/login.ejs'
                });
            }
        })
    })
};

clienteController.ingresar = (req, res) =>{
    
    const {userLogin, claveLogin} = req.body;
    
    if( !userLogin || !claveLogin){
        return res.status(400).render('login',{
            alert: true, mensaje: "Campos vacios", icono: "info", time: 1500, ruta: '/login.ejs'
        })
    }

    conn.db.query('SELECT * FROM Cliente WHERE usuario = ? AND contraseña = ?', [userLogin, claveLogin], function(err, result){
        if(result.length > 0){
            return res.render('_Usuario', {
                //alert: true, mensaje: "Registro completado con exito", icono: "success", time: 1500, ruta: '/_Usuario.ejs'
                alerta: 'Bienvenido'
            })
        }
        else{
            return res.render('login', {
                alerta: 'xD los datos ingresados son incorrectos'
            })
        }
    })
}

/*clienteController.ingresar = async (req, res) => {
    try{
        const {userLogin, claveLogin} = req.body;

        if( !userLogin || !claveLogin){
            return res.status(400).render('index')
        }

        conn.db.query('SELECT * FROM `Cliente` WHERE usuario = ?', [userLogin], async (err, results) => {
            console.log(results)
            if(!results || !(await bcrypt.compare(claveLogin, results[0].contraseña) ) ){
                res.send('INCORRECTOS')
            }
            else{
                res.send('kjj')
            }
        })
    } catch (error) {
        console.log(error)
    }
}*/

module.exports = clienteController;