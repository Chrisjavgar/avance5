var productos = document.getElementById('productos');
var templateProducto = document.getElementById('templateProducto').content

var detalleCarrito = document.getElementById('detalleCarrito');
var footerCarrito = document.getElementById('footerCarrito');
var templateCarrito = document.getElementById('template-carrito').content
var templateFooter = document.getElementById('template-footer').content

var fragment = document.createDocumentFragment();

var carrito = {}; // OBJETO CARRITO


document.addEventListener('DOMContentLoaded', () =>{
    dbJson()

    if(localStorage.getItem('carrito')){
        carrito = JSON.parse(localStorage.getItem('carrito'))
        mostrarCarrito()
    }
})

productos.addEventListener('click', e =>{
    agregarCarrito(e)
})

detalleCarrito.addEventListener('click', e =>{
    btnAccion(e);
})


/* BASE DE DATOS TIPO JSON, XD */
const dbJson = async () => {
    try{
        const res = await fetch('/json/db.json')
        const datos = await res.json()
        console.log(datos)
        mostrarProductos(datos)
    }
    catch(error){
        console.log(error)
    }
}

/* MOSTANDO PRODUCTOS */
const mostrarProductos = datos => {
    //console.log(datos);
    datos.forEach(productos => {
        templateProducto.querySelector('h5').textContent = productos.nombre;
        templateProducto.querySelectorAll('p')[0].textContent = productos.precio;
        templateProducto.querySelectorAll('p')[1].textContent = productos.descripcion;
        templateProducto.querySelector('img').setAttribute('src', productos.img);
        templateProducto.querySelector('.btn-primary').dataset.id = productos.id;
        const clone = templateProducto.cloneNode(true);
        fragment.appendChild(clone);
    });
    productos.appendChild(fragment)
}

// Agregando al carrito
const agregarCarrito = e => {
    //console.log(e.target);
    //console.log(e.target.classList.contains('btn-primary'))
    if(e.target.classList.contains('btn-primary')){
        obtenerProducto(e.target.parentElement)
    }
}

// Capturando producto
const obtenerProducto = obtenido =>{
    //console.log(obtenido);
    const productos = {
        id: obtenido.querySelector('.btn-primary').dataset.id,
        nombre: obtenido.querySelector('h5').textContent,
        precio: obtenido.querySelector('p').textContent,
        cantidad: 1
    }

    if(carrito.hasOwnProperty(productos.id)){
        productos.cantidad = carrito[productos.id].cantidad + 1;
    }

    carrito[productos.id] = {...productos}
    console.log(productos);
    mostrarCarrito();

    alert('Producto agregado al carrito')
}

// Productos del carrito
const mostrarCarrito = () => {
    detalleCarrito.innerHTML = '';
    Object.values(carrito).forEach(productos =>{
        templateCarrito.querySelector('th').textContent = productos.id
        templateCarrito.querySelectorAll('td')[0].textContent = productos.nombre
        templateCarrito.querySelector('.btn-info').dataset.id = productos.id
        templateCarrito.querySelector('.btn-danger').dataset.id = productos.id
        templateCarrito.querySelectorAll('td')[2].textContent = productos.cantidad
        templateCarrito.querySelector('span').textContent = productos.cantidad * productos.precio

        const clone = templateCarrito.cloneNode(true)
        fragment.appendChild(clone)
    })

    detalleCarrito.appendChild(fragment);
    footerMensaje();

    localStorage.setItem('carrito', JSON.stringify(carrito))
}

// Totales de carrito
const footerMensaje = () => {
    footerCarrito.innerHTML = ''
    if(Object.keys(carrito).length === 0){
        footerCarrito.innerHTML = `
            <th colspan="5">Carrito sin productos</th>
        `
        return
    }

    //const nCantidad = Object.values(carrito).reduce((acc, { cantidad }) => acc + cantidad, 0)
    const nPrecio = Object.values(carrito).reduce((acc, { cantidad, precio }) => acc + cantidad * precio, 0)

    const pIva = nPrecio * 0.12;
    const total = pIva + nPrecio;

    /*const desct = total * 0.10;
    const totalf = total - desct;
    console.log(totalf)*/

    //templateFooter.querySelectorAll('td')[1].textContent = nCantidad;
    templateFooter.querySelectorAll('span')[0].textContent = nPrecio
    templateFooter.querySelectorAll('span')[1].textContent = pIva
    templateFooter.querySelectorAll('span')[2].textContent = total

    const clone = templateFooter.cloneNode(true)
    fragment.appendChild(clone)
    footerCarrito.appendChild(fragment)

    const vaciarCarrito = document.getElementById('vaciar-carrito')
    vaciarCarrito.addEventListener('click', () => {
        carrito = {}
        mostrarCarrito()
    })
}

// Aumentar y Disminuir producto
const btnAccion = e =>{
    if(e.target.classList.contains('btn-info')){
        console.log(carrito[e.target.dataset.id])
        const productos = carrito[e.target.dataset.id]
        productos.cantidad++
        carrito[e.target.dataset.id] = {... productos}
        mostrarCarrito();
    }

    if(e.target.classList.contains('btn-danger')){
        console.log(carrito[e.target.dataset.id])
        const productos = carrito[e.target.dataset.id]
        productos.cantidad--
        if(productos.cantidad === 0){
            delete carrito[e.target.dataset.id]
        }
        mostrarCarrito();
    }
}

/* VALIDANDO FORMULARIO DE COMPRA */
//Expresiones Regulares

var expresiones_nombre = /^[a-zA-Z ]{4,20}$/;
var expresiones_direccion = /^[a-zA-Z ]{4,40}$/;
var expresiones_telefono = /^[0-9]{10}$/

//Llamamos el formulario de compra
var validarLogin = document.getElementById("formulario-compra");

//Funcion de validacion de formulario de compra
validarLogin.onsubmit = function(){
    let nombre = document.getElementById('nombre').value;
    let direccion = document.getElementById('direccion').value;
    let telefono = document.getElementById('telefono').value;

    if(Object.keys(carrito).length === 0){ // Validacion de carrito vacio
        alert('Aun no tienes productos en tu carrito - Vamos a comprar :)')
    }

    else if(nombre == "" || direccion == "" || telefono == ""){ // Validacion de campos vacios
        alert("Por favor, rellene el formulario de compra");
        return false;
    }

    else if(!expresiones_nombre.test(nombre)){
        alert('Nombre esta mal escrito')
        return false;
    }

    else if(!expresiones_direccion.test(direccion)){
        alert('Direccion esta mal escrito')
        return false;
    }

    else if(!expresiones_telefono.test(telefono)){
        alert('Telefono incorrecto')
        return false;
    }

    else{
        alert('Gracias por comprar con nosotros')
        localStorage.removeItem("carrito")
        footerMensaje()
    }
}