  var panorama;

  function initialize() {
      panorama = new google.maps.StreetViewPanorama(
          document.getElementById("street-view"), {
              position: {
                  lat: -0.96212,
                  lng: -80.71271,
              },
              pov: {
                  heading: 165,
                  pitch: 0,
              },
              zoom: 1,
          }
      );
  }